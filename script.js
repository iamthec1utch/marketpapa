/**
 * Функция, в которой отрабатывает вся логика расширения
 */
function createMarketPapaExtension() {
    // Артикул товара на текущей страницы. Нужен для обновления информация о товаре при переходе с одной страницы на другую
    let currentArticle = null;

    // Создаем контейнеры с информацией о СПП и складах выбранного товара
    const priceContainer = document.createElement('div');
    priceContainer.classList.add('price-container');
    // Копия для адаптивного блока информации о СПП
    let priceContainerCopy = document.createElement('div');
    priceContainerCopy.className = 'price-container-adaptive';
    const stocksContainer = document.createElement('div');
    stocksContainer.classList.add('stocks-container');

    /**
     * Обработчик, который вызывается при наблюдении мутаций DOM.
     *
     * @param {MutationRecord[]} mutationsList - Массив записей мутаций, представляющих изменения DOM.
     * @param {MutationObserver} observer - Объект MutationObserver, который вызывает эту функцию.
     */
    function observableFunction(mutationsList, observer) {
        for (const mutation of mutationsList) {
            if (mutation.type === 'childList') {
                mutation.addedNodes.forEach(async (node) => {
                    // Проверяем, что блок, на котором завязана отрисовка логики, уже появился в DOM
                    if (document.querySelector('.product-page__seller-wrap.section-border')
                        && document.querySelector('.product-page__seller-wrap.section-border').querySelector('.seller-info__content')
                        && currentArticle !== document.getElementById("productNmId").textContent) {
                        currentArticle = document.getElementById("productNmId").textContent;
                        priceContainer.innerHTML = "";
                        priceContainerCopy.innerHTML = "";
                        stocksContainer.innerHTML = "";
                        // Создаем промис, в котором фетчим необходимую информацию о товаре
                        const productPromise = new Promise(function (resolve, reject) {
                            const article = document.getElementById("productNmId").textContent;
                            const API_URL = "https://card.wb.ru/cards/detail?appType=1&curr=rub&dest=-1257786&regions=80,83,38,4,64,33,68,70,30,40,86,75,69,1,66,110,22,48,31,71,112,114&spp=29&nm=" + article;
                            fetch(API_URL, {
                                method: "GET",
                                headers: {
                                    "Content-Type": "application/json",
                                },
                            })
                                .then(function (response) {
                                    return response.json();
                                })
                                .then(function (dataAll) {
                                    const dataSet = dataAll.data.products;
                                    const price = dataSet[0].extended.clientPriceU / 100 || dataSet[0].extended.basicPriceU / 100;
                                    const spp = dataSet[0].extended.clientSale || 0;
                                    const stocks = [];
                                    dataSet[0].sizes.forEach(size => {
                                        stocks.push({
                                            name: size.origName,
                                            stocks: size.stocks,
                                            time: Number(size.time1) + Number(size.time2),
                                            id: size.wh}
                                        );});

                                    resolve({price: price, spp: spp, stocks: stocks});
                                })
                                .catch(function (error) {
                                    reject(error);
                                });
                        });

                        const product = await productPromise;

                        // Создаем промис, в котором получаем информацию о складах. Это нужно, чтобы сопоставить id склада в информации о товаре с его названием
                        const stocksPromise = new Promise(function (resolve, reject) {
                            const API_URL = "https://static-basket-01.wb.ru/vol0/data/stores-data.json";
                            fetch(API_URL, {
                                method: "GET",
                                headers: {
                                    "Content-Type": "application/json",
                                },
                            })
                                .then(function (response) {
                                    resolve(response.json());
                                })
                                .catch(function (error) {
                                    reject(error);
                                });
                        });

                        const stocks = await stocksPromise;

                        // Сортируем склады для текущего товара по количеству часов (чтобы итоговая таблица раскладки по складам была более читаема)
                        // и присваиваем каждому складу имя, на основе сфетченной ранее информации
                        const sortedStocks = product.stocks.map(currentSizeStocks => {
                            currentSizeStocks.stocks.sort((a, b) => Number(a.time1 + a.time2) > Number(b.time1 + b.time2) ? 1 : -1);
                            currentSizeStocks.stocks.forEach(stockInfo => {
                                stockInfo.title = stocks.find(stock => stock.id === Number(stockInfo.wh)).name.replaceAll("WB", '').replaceAll("склад продавца", "FBS").replaceAll("Склад продавца", "FBS").trim();
                            });
                            return currentSizeStocks;
                        });

                        // Находим блок, над которым будем распологать контейнер с информацией о складах
                        const sellerBlock = document.querySelector('.j-price-block');
                        const currentSizeBlock = document.querySelector('.sizes-list__button.active');
                        // Проверяем наличие различных размеров / комплектации товара
                        const sizeButtons = document.querySelectorAll('.sizes-list__button');

                        // Определяем выбранный размер (если не выбран, то по умолчанию показывается информация для первого из списка размеров / комплектации)
                        let selectedSize = "0";
                        if (sizeButtons.length !== 0 && product.stocks[0].name !== "0") {
                            selectedSize = currentSizeBlock ? currentSizeBlock.querySelector('span.sizes-list__size').textContent : Array.from(sizeButtons).find(button => !button.classList.contains('disabled')).querySelector('.sizes-list__size').textContent;
                        }

                        // Добавляем обработчик событий переключения размеров, чтобы перерисовывать информацию для выбранного размера / комплектации
                        sizeButtons.forEach(button => {
                            button.addEventListener('click', function () {
                                const sizeSpan = button.querySelector('.sizes-list__size');
                                selectedSize = sizeSpan.textContent;
                                let intervalId = setInterval(function() {
                                    const priceBlockContent = document.querySelector('.price-block__content');
                                    if (priceBlockContent || button.classList.contains('disabled')) {
                                        clearInterval(intervalId);
                                        // Если размер указан в товаре, но отсутствует в продаже, то информацию не выводим
                                        if (!button.classList.contains('disabled')) {
                                            updatePriceContainers();
                                        } else {
                                            document.querySelector('.stocks-container').style.display = 'none';
                                            document.querySelector('.price-container').style.display = 'none';
                                        }
                                    }
                                }, 100); // Интервал проверки каждые 100 миллисекунд
                            });
                        });

                        const sppValue = product.spp;
                        const price = product.price;
                        const withoutDiscount = Math.ceil(price / (1 - 0.01 * sppValue)).toLocaleString('ru-RU');

                        const logo = document.createElement("img");
                        logo.classList.add('logo');
                        // К сожалению, не получилось реализовать таким способом, хотя в devtools путь к картинке был корректным =(
                        // logo.src = chrome.runtime.getURL("images/logo.png");
                        logo.src = "https://i.ibb.co/VQsmvRL/logo.png";
                        logo.alt = "Market Papa logo";
                        priceContainer.appendChild(logo);
                        priceContainer.innerHTML += `СПП: <p style="color: black; font-size: 18px">${sppValue}%</p> <p style="font-weight: normal">До СПП: </p><p style="font-size: 18px; font-weight: 500">${withoutDiscount}₽</p>`;
                        updatePriceContainers();

                        function updatePriceContainers () {
                            // Находим 2 блока с ценами (один для экранов > 1366px, второй - меньше)
                            // и добавляем наш priceContainer, чтобы информация корректно и адаптивно отображалась на различных экранах
                            const priceBlockContentAside = document.querySelector('.product-page__price-block.product-page__price-block--aside');
                            const priceBlockContent = document.querySelector('.product-page__price-block.product-page__price-block--common');
                            const priceBlock = priceBlockContent.querySelector('.price-block')
                                ? priceBlockContent.querySelector('.price-block')
                                : priceBlockContent;

                            if (priceBlock !== priceBlockContent) {
                                priceBlockContentAside.insertAdjacentElement("afterend", priceContainer);
                                const priceContainerAdaptive = document.querySelector('.price-container-adaptive');
                                if (priceContainerAdaptive) {
                                    priceContainerAdaptive.remove();
                                }

                                priceContainerCopy = priceContainer.cloneNode(true);
                                priceContainerCopy.className = 'price-container-adaptive';
                                priceBlock.insertAdjacentElement("afterend", priceContainerCopy);

                                document.querySelector('.price-container').style.display = '';
                                document.querySelector('.price-container-adaptive').style.display = '';


                                // Добавляем информацию на созданный контейнер о выбранном размере / комплектации товара
                                // или, если у товара один размер / комплектация, то о нем
                                updateStocksContainer(selectedSize);
                            }
                        }

                        // Обновление информации о контейнере со складами вынесено в отдельную функцию
                        // т.к. товары разных размеров / комплектации хранятся в различном количестве на разных складах
                        function updateStocksContainer(selectedSize) {
                            // Очищаем содержимое контейнера
                            stocksContainer.innerHTML = '';

                            const headerStockDiv = document.createElement('div');
                            headerStockDiv.classList.add('stocks-header');
                            headerStockDiv.appendChild(logo);
                            headerStockDiv.innerHTML += `<div class="stocks-title">Раскладка по складам</div>`;

                            const bestStockP = document.createElement('p');
                            bestStockP.classList.add('best-stock');

                            const selectedSizeStocks = sortedStocks.find(sizeStocks => sizeStocks.name === selectedSize)
                                ? sortedStocks.find(sizeStocks => sizeStocks.name === selectedSize).stocks
                                : sortedStocks[0];
                            const bestStock = selectedSizeStocks.stocks ? selectedSizeStocks.stocks[0] : selectedSizeStocks[0];
                            bestStockP.innerHTML = bestStock.title + `: ${Number(bestStock.time1) + Number(bestStock.time2)} час. ☆`;

                            // Теперь создаем таблицу со складами
                            const tbody = document.createElement('tbody');
                            for (let i = 0; i < selectedSizeStocks.length; i++) {
                                const row = document.createElement("tr");
                                const cell1 = document.createElement("td");
                                cell1.innerHTML = selectedSizeStocks[i].title + ":";
                                const cell2 = document.createElement("td");
                                cell2.style.fontWeight = 'bold';
                                cell2.innerHTML = String(Number(selectedSizeStocks[i].time1 + selectedSizeStocks[i].time2)) + " ч.";
                                const cell3 = document.createElement("td");
                                cell3.innerHTML = selectedSizeStocks[i].qty + " шт.";

                                row.appendChild(cell1);
                                row.appendChild(cell2);
                                row.appendChild(cell3);
                                tbody.appendChild(row);
                            }

                            const table = document.createElement('table');
                            table.appendChild(tbody);

                            stocksContainer.appendChild(headerStockDiv);
                            stocksContainer.appendChild(bestStockP);
                            stocksContainer.appendChild(table);

                            sellerBlock.insertAdjacentElement("afterend", stocksContainer);
                            document.querySelector('.stocks-container').style.display = '';
                        }
                    }
                });
            }
        }
    }

    const targetNode = document.body;
    const config = { childList: true, subtree: true };
    const observer = new MutationObserver(observableFunction);

    observer.observe(targetNode, config);
}

document.readyState === "loading" ? document.addEventListener("DOMContentLoaded", createMarketPapaExtension) : createMarketPapaExtension();